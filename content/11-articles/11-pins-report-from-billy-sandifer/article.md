Title: PINS Report from Billy Sandifer
----
Date: 09/12/2012
----
Tags:
----
Text:

I guess today would be a prime example of the reason this “riding fence” is most worthwhile in my mind and exemplifies things we learn in the process.

Jeff Wolda, the new guide on PINS, and I started at the end of the pavement at 07:30 a.m. Seas were clear and 2 ft. in height and small organic debris was continuous along the entire shoreline. Apparently from Hurricane Isaac. Very few birds were seen on the way south other than staging flocks of Sanderlings and occasional mixed shorebirds.

A 10 point buck White-tailed Deer was dead in the surf at the 36 mile with no visible wounds and bloated but fresh. 1,200 yards north of the Turtle Shack at the 39 mile we encountered a heavy crude oil spill 10′ to 30′+ wide which continued 800 yards South; almost to the 40 mile. At the 46 mile we encountered 800 yards of heavy crude oil and at the 49 mile beachfront we encountered 1,300 yards. They were too liquid to be actually called “balls”.