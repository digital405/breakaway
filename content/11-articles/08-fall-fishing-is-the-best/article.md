Title: Fall Fishing is the Best
----
Date: 10/15/2012
----
Tags:
----
Text:

This is the time of year that a lot of old fishermen will tell you is the best time of year. From now until Christmas is harvest time. Need to be dusting off your rods and reels and make an effort to get after it.

The Mullet run is in full swing now and large numbers of Reds have been caught at the passes. On the calm days, good catches of Pompano are being reported. So far this year we have been very fortunate in that we have not had a serious Red Tide event allowing our resident target fish to hopefully recuperate.

We did hear a little tippet about weed actually removing nitrates from the water. I called Tony Amos on this and he said he will look into it. The Sargasso that has inundated our beaches might be one of the few things protecting us from the Dead Zone and Red Tide, learn to love your weed.

Ok, the run is on and hopefully I get to see you all soon at the shop. The [Breakaway DCD3 Double Clip Down Rig](/leaders/double-clip-down/) is the most popular rig this time of year. Killer on the Pompano and you can put some big Reds to the beach with those small circle hooks. The [Breakaway Clip Down Pulley Rig](/leaders/clip-down-pulley/) is the number one rig for long casting live mullet.

Great picture of Brenda with a nice fall Red caught on PINS.