Title: Offshore Report from Nathan Harwell
----
Date: 07/02/2012
----
Tags:
----
Text:

Got this great offshore report from Nathan at [Port A Sport Fishing](www.portasportfishing.com).

Hello all at Breakaway,

Although I run charters offshore, I am not going to ‘sugarcoat’ this report as you see in many reports these days……..So far this offshore fishing season, the weather has been generally rough to occasionally ” bouncy” , and the fish nearshore (within 35 miles) have been scattered and sometimes elusive. Several low pressure systems have moved through since May, which doesn’t help the bite or the ride, however the fishing before these systems on a falling barometer has been red hot. There has been a rip running offshore of the Baker Rigs holding lots of wahoo, mahi, and blues, if you can make it out there. There were a few weedlines holding wahoo around Southern Rock and Ant Hill Rigs, but the windows to fish before being broken up have been small. Extremly high tides over the past several weeks have set up for a dangerous ebb tide when heading back through the jetties, use extreme caution especially on rougher days.

As far as fishing for specific species -Ling have not been too thick since the early run we saw in April. We’ve managed to boat most every one that surfaces, but again we just havn’t seen that many hanging around, yet. The red snapper bite has been hot at times but there have been days lately when you had to really work for that limit. We sat on top of a ball of snapper the other day that were all running 14 lbs or better and our clients wore themselves out just on their limit. Snapper season has been extended a few days until July 17th. Mangrove snapper fishing has been really good this season, with many in the 5-10 lb class being landed, and many 14lb + being broken off on the platform, absolutley one of the hardest fighting fish per/pound. Kings have not really shown up in numbers nearshore, but when the shrimpers start fishing (july 15) and the weather calms down they’ll show up to the party. Several sails were caught and jumped about a week ago when there was an intense color-change about 10 miles offshore of the beach, within 25 yards the turquoise shifted to clean green water.

The best is yet to come for this season I believe.

See ya out there!

Nathan Harwell