Title: New HDX Brings His Biggest Red
----
Date: 10/29/2012
----
Tags:
----
Text:

Just got this great email from one of our customers.

Just wanted to say thanks for the unbelievable rod that you made for me this week. Picked it up on Friday and baptized it Saturday morning.

Wow does it work fine. Not only does it cast further but it fought this huge red (my personal best) just awesome. Took about 20 minutes to land him.

The rod had a perfect bend in it with plenty of backbone to win the fight.

THANKS!!!