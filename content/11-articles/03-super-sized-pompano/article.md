Title: Super Sized Pompano
----
Date: 11/08/2012
----
Tags:
----
Text:

Look at this Pompano I caught last night, 6.25lb. The state record is only slightly bigger. If it had been 7lbs, I would have brought it in for weighing. I was using a [Breakaway HDX](/rods/hdx/) with a [Daiwa SHA 30](/reels/daiwa/casting/sha/), a [Breakaway DCD3 Double Clp Down Rig](/leaders/double-clip-down/), and a [Breakaway 5oz Super Sinker](/tackle/weights/). 

The bait we were using was fresh shrimp and Fishbites. 

Get out there everyone, plenty of good Pompano to be had.