Title: Great Report from Jay Cisneros
----
Date: 08/28/2012
----
Tags:
----
Text:

Jay sent me this great picture and report of a jointly caught Red Fish using the new Lure Links and mini swivels.

Love the smile on your boys face.

>Nick,

>I bought a pack of the lure links with the swivel on Friday afternoon. I went down from Bob Hall Pier Saturday and got this nice 29.5” red with a little help from my son. The lure link worked great making it easy to try different colors and weights of spoons.

>Thanks,
Jay Cisneros