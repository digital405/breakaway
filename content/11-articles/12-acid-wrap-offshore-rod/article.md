Title: Breakaway Acid Wrap Offshore Rod
----
Date: 09/07/2012
----
Tags:
----
Text:

I got some of the new blanks in from our supplier and Claus wanted to have a go at building an Acid Wrap rod on the 40 to 65lb blank.

He did the wrapping at the shop and did a great job. It worked out great and here are some pictures of the rod and a [short video of the rod in action](https://www.facebook.com/photo.php?v=480623611956680).

