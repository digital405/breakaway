Title: A Great Email from a Happy Customer
----
Date: 09/21/2012
----
Tags:
----
Text:

We just got this great email from a happy customer.

I am having so much fun with my [Breakaway LDX](/rods/ldx/) rod and [Akios 656CSM](/reels/akios/) reel. Look at my big Butt. This halibut was nearly 100 yds out. It was 34″ long. I did not keep it because it was breeding size. We want to protect the future of our fishing. However I would keep a smaller fish.

I credit your rod and your teaching video for the catch.

Thank You
Don