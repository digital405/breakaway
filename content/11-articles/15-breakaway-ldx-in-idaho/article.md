Title: Breakaway LDX in Idaho
----
Date: 07/11/2012
----
Tags:
----
Text:

Check out this great report we got from Idaho.

Here’s why we need surf rods in Idaho. You might be getting a couple calls. Several people were writing down your info. The Emblem Shot with the 4500 spools are perfect. 300 yds 20 lb. braid and 30′ of mono shock for the slip float to ride on.

100+ yd. throws any time you want. Or as we say at the dam “I should land this just beyond you.”

Thanks for the great service.