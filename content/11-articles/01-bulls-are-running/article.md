Title: Bulls Are Running
----
Date: 11/26/2012
----
Tags:
----
Text:

Some nice Reds running PINS at the moment. The fishing has been awesome this year with some quality Pompano up to 6lb and Reds over 45 inches. The wife and I landed 18 Pompano in just over an hour last week. Sunday was slower but the fishing was still great. Pompano are being caught on fresh dead shrimp and Fishbites. The big Reds are falling to cut bait.

The best rods for Pompano are the [Breakaway LDX Spinning](/rods/ldx/) with a [Daiwa Spinning Reel](/reels/daiwa/spinning/) and the [Breakaway HDX Casting](/rods/hdx/) with a [Daiwa 30 SHA](/reels/dawia/casting/sha/).

The rigs we used and reccoend are the [Breakaway Clip Down Pully Rig](/leaders/clipidown-pulley/) for Reds with cut bait and the [Breakaway DCD3 Double Clip Down Rig](/leaders/double-clip-down/) for Pompano.