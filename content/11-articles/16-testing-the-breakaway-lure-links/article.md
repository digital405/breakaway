Title: Testing the Breakaway Lure Links
----
Date: 07/10/2012
----
Tags:
----
Text:

Wanted to give the new [Breakaway Lure Links](/tackle/lure-links/) a test and some new super small swivels we have coming in.

To give me a bit of extra line on a small bait casting reel I put about 50 yds of 40lb braid, then go to 17lb running line, finishing with 12inches of 30lb fluorocarbon. The rod I was using was the [Breakaway Alpha](/rods/alpha/) matched up to the [Daiwa 100 HA](/reels/daiwa/100ha/) reel.

My first stop was at a suck-out about three miles south on the Padre Island National Seashore. I could see schools of Pony Mullet cutting out through the suck out. I waded across the gut and maneuvered so I could fish on the outside of the washout in the bar. Using a top water, I had two blow-ups on the first two casts; more like tail slaps. I had some new sinker lures sent to me from Dave Kiddy in the UK. The lures are sold under the name [Sidewinder](http://sidewinderlures.co.uk/) in the UK and are very effective and have a great life-like swimming action.

This time of year it is not unusual to find big schools of Reds heading south. Normally, they will be slots and if you find them you will limit out pretty quick. We had a plan to go find them if we could. Passing a friend in the 20′s, I told him what our plan was and continued south. We were looking for the signs: birds, structure and bait. We found them and the Reds. Every cast had Reds between the sizes of 22 to 27inches.  I was using lures again and Brenda was using dead bait. They were hungry and the bite was on. The [Breakaway Lure Links](/tackle/lure-links/) were more than a match for the large reds which do put up a great fight.

Finding the fish was a major part of the game this weekend. Check out my video “Reading the Beach” at our [Nickaway Media](http://nickaway.com/) website.

(vimeo: https://vimeo.com/43675277 width: 720 height: 405)