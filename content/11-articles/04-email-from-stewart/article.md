Title: Great Email and Report From Stewart
----
Date: 11/06/2012
----
Tags:
----
Text:

I received this wonderful email recently from a new customer.

Nick, this was our first trip out with your gear. We went to the Texas City Dike hunting for Bull Red and we found them. We caught a 35 and 32 pound red. This was the first time we had ever been this successful and we were the only people catching big fish. There is no doubt in my mind it was because of your equipment. I was using the [HDX](/rods/hdx/) rod and [Diawa SH30](/reels/daiwa/casting/sha/) you modified for me with only 20# mono. I was also using your [Super Sinker](/tackle/weights/) with a [Clip Down Pulley Rig](/leaders/clip-down-pulley/) and a large [Speed Oval](/tackle/speed-ovals/).

This rig worked like a dream for three reasons:

###Rod is incredible
We were catching 4″ – 7″ Perch with squid near the rocks. We would then hook the fish through the back so it would stay alive and throw it out there. This was my first time using the rod so my casting abilities are sure to improve. Still we were able to get the bait much further out than anyone else fishing around us even given the heavy weight of the Perch. The distance made the difference! I must also say the rod handled the fights with no problems. There is no doubt this rod can land BIG fish!

###Clip Down Pulley Rig
I didn’t realize from your website what the ‘pulley’ part meant but now I know. This worked fantastic with a heavy live fish for bait allowing me to put the loop holding the hook on the impact shield truly creating a nearly aerodynamic rig. I think the play in the ‘pulley’ worked great with the fish too giving me more time to set the hook after the fish began running with the bait.

###Less lost rigs
We fished for over 12 hours and I only lost one pulley rig the whole time and it was my fault for not pulling the bait in when a large tanker/barge was going by. This was amazing. I attribute this to the sinker and the fact the long cast allows you to get past so many rocks and other people’s lost rigs/fishing line. There is no doubt I will buy all of my rigs from you in the future, nothing else compares.

Nick I appreciate all of your help. You are not just selling rods and rigs, you are selling a complete system for casting long distance and landing big fish!

Best regards