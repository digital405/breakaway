Title: Testing the Breakaway Speed Oval
----
Date: 07/06/2012
----
Tags:
----
Text:

My friend Klaus sent me this great offshore report and a test for our new [Breakaway Speed Ovals](/tackle/speed-ovals/).

Fishing offshore on one of the head boats, using lures attached to the running line via our new [Breakaway Speed Oval](/tackle/speed-ovals/) clips.

Klaus landed two nice Kings, a couple of Snapper, and a nice 24lb Black fin Tuna.

It was a great test for the clips which they passed with flying colors. Klaus told me that he was handing the clips out to other fishermen who wanted to try them.

Unfortunately, whilst fighting the Tuna, Klaus slipped and fell breaking his wrist. He did land the Tuna so he was not too upset by the broken wrist.

Thanks for the great report and pictures mate hope you get back to fit soon.