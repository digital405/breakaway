Title: Breakaway HDX-T
----
Details:

Blank:
  Style: Blank
  Length: 14'
  Line:   20 - 30lb
  Lure:  4 - 8oz
  Price:  $273.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+hdx-t+Blank&price=273.99&units=21&return=www.breakawayusa.com/rods/hdx-t/
----
Vimeo: 45917228
----
Text:

##Breakaway HDX-T Rod Blank
- 14ft two piece blank
- Blank rod tip
- X-flock shrink tube on butt
- Great long distance rod

We can wrap the rod guides on to customer spec, just call us for a quote.