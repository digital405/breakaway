Title: Breakaway LDX
----
Details:

Casting:
  Style: Casting
  Length: 13'
  Line:   15 - 25lb
  Lure:  3 - 5oz
  Price:  $209.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+ldx+Casting&price=209.99&units=21&return=www.breakawayusa.com/rods/ldx/
Spinning:
  Style: Spinning
  Length: 13'
  Line:   20 - 30lb
  Lure:  3 - 5oz
  Price:  $209.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+ldx+Spinning&price=209.99&units=21&return=www.breakawayusa.com/rods/ldx/
----

Text:
## Breakaway LDX 13′ Longcasting Rod
With endless effort and time put into the design of the new LDX series rods, long casting has been re-invented for the true surf fishermen. The latest in durable lightweight graphite technology combined with the designs by Nick Meyer make this rod an absolute winner for long casting or surf fishing!