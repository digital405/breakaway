Title: Rods
----
Text:

##Breakaway Surf &amp; Light Tackle Rods

Breakaway Tackle and Development is responsible for some of the longest distance tournament casting and fishing rods ever made. Ranging from your average trout and redfish “poppin” rods to the the ultimate in long casting tournament and surf fishing rods, we custom make all rods to fit the individuals needs.

Part of the secret to our success is the amount of time that people who compete in tournaments practice on the field. Breakaway rods have achieved distances in excess of 770+ ft. in tournament. We can also say that we have taken more beginners in the sport of long casting in the USA beyond 600ft in the last 2 years than any other rod manufacturer.

###Construction

All Breakaway rods are built using RX7 graphite and zoned action. In the old days, rods were designed where the action would go completely through the blank, ie: when the rod was loaded the action of the rod would go all through the rod including the butt. The newer "zoned action" rods have got three separate zones that come into play as the rod is loaded.

First the tip loads and then the action moves down the rod into the middle zone. Finally the action goes to the butt section were the energy is stored. The butt on the Breakaway rods stay very stiff when the rod has achieved lock down (lock down is when the tip is 90 degrees to the butt of the rod). The energy is then released from the butt of the rod towards the tip. The powerful butt will recover much faster than the old rods. This increases the distance you will cast.

###Choosing a Rod

<table>
	<thead>
		<tr>
			<th scope="col">Rod</th>
			<th scope="col">Gut Area</th>
			<th scope="col">Cast Weight</th>
			<th scope="col">Target Fish</th>
			<th scope="col">Size Limit</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Alpha 7'6"</td>
			<td>1</td>
			<td>1/4 - 1oz</td>
			<td>Whiting, Trout, Reds</td>
			<td>40"</td>
		</tr>
		<tr>
			<td>Omega 10'6"</td>
			<td>1 &amp; 2</td>
			<td>3/4 - 2oz</td>
			<td>Jacks, Reds, Trout</td>
			<td>40"</td>
		</tr>
		<tr>
			<td>LDX 13'</td>
			<td>2 &amp; 3</td>
			<td>3 - 5oz</td>
			<td>Pomps, Jacks, Reds</td>
			<td>5ft</td>
		</tr>
		<tr>
			<td>HDX 13'</td>
			<td>3</td>
			<td>4 - 8oz</td>
			<td>Pomps, Reds, Shark</td>
			<td>7ft</td>
		</tr>
	</tbody>
</table>

