Title: Breakaway HDX
----
Details:

Casting:
  Style: Casting
  Length: 13'
  Line:   20 - 30lb
  Lure:  4 - 8oz
  Price:  $249.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+hdx+Casting+Rod&price=249.99&units=21&return=www.breakawayusa.com/rods/hdx/
Spinning:
  Style: Spinning
  Length: 13'
  Line:   20 - 30lb
  Lure:  4 - 8oz
  Price:  $249.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+hdx+Spinning+Rod&price=249.99&units=21&return=www.breakawayusa.com/rods/hdx/
----
Youtube: 3-DIkwL8FYo
----
Text:

##Breakaway HDX 13′ Longcasting Rod
With the successful release of the LDX, we went back to the drawings boards to bring you a heavier version of the LDX. The new HDX is an all purpose heavy action surf casting rod also being widely used and successfully field tested in tournament casting.