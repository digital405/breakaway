Title: Breakaway Alpha
----
Details:

Casting:
  Style: Casting
  Length: 7'6
  Line:   8 - 12lb
  Lure:  1/8 - 3/4oz
  Price:  $133.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+Alpha+Casting+Rod&price=133.99&units=21&return=www.breakawayusa.com/rods/alpha/

Spinning:
  Style: Spinning
  Length: 7'6
  Line:   8 - 12lb
  Lure:  1/8 - 3/4oz
  Price:  $133.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+Alpha+Spinning+Rod&price=133.99&units=21&return=www.breakawayusa.com/rods/alpha/
----
Youtube: p-RFuw4nGT4
----
Text:

##Breakaway Alpha Light Tackle Rod

Breakaway Tackle brings you the newest innovation in light tackle gear. The Alpha is the newest in a line of superior Breakaway rods designed for trout, redfish, and various light tackle saltwater fishing. The Alpha is available in both casting and spinning versions at a modest price.