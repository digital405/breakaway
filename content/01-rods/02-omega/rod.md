Title: Breakaway Omega
----
Details:

Blank:
  Style: Blank
  Length: 10'6
  Line:   15 - 25lb
  Lure:  2 - 5oz
  Price:  $119.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+Omega+Blank&price=119.99&units=21&return=www.breakawayusa.com/rods/omega/
Casting:
  Style: Casting
  Length: 10'6
  Line:   15 - 25lb
  Lure:  2 - 5oz
  Price:  $239.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+Omega+Casting+Rod&price=239.99&units=21&return=www.breakawayusa.com/rods/omega/
Spinning:
  Style: Spinning
  Length: 10'6
  Line:   15 - 25lb
  Lure:  2 - 5oz
  Price:  $239.99
  Link: http://ww8.aitsafe.com/cf/add.cfm?userid=A499394&product=Breakaway+Omega+Spinning+Rod&price=239.99&units=21&return=www.breakawayusa.com/rods/omega/
----
Vimeo: 27514627
----
Text:

##Breakaway Omega 10′6 Fast Action Rod
The Omega is a versatile 2 piece, 10 1/2′ rod designed for lightness and strength. With a fast action and firmer butt, it is the perfect rod for throwing long range with medium/heavy class lures and spoons. Buy the blank and customize to your needs or have us custom build a rod for you (estimated completion time for custom rods is 2 weeks).