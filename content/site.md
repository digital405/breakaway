Title: Breakaway Tackle & Development
----
Author: Nick Meyer test
----

Description: Breakaway Tackle and Development is centrally located in Corpus Christi, the heart of the Texas coastline. For years now, Nick Meyer and the Breakaway team have specialized in helping anglers with productive techniques and equipment to help improve their casting and fishing abilities. The friendly Breakaway staff will help guide you to the most logical equipment choice.
----
Copyright: © 2009-(date: Year) (link: http://www.breakawayusa.com text: Breakaway Tackle & Development)
